#include "Game.h"
#include <iostream>
#include "List.h"

// human = 1, bot = 2

Game::Game(int board_size, int row_win) {
  this->size_of_map = board_size;
  this->row_win = row_win;
  field = new int[board_size * board_size];

  for (int i = 0; i < board_size * board_size; ++i) {
    field[i] = 0;
  }
}

int Game::getBoardSize() const {
  return size_of_map;
}

int Game::getRowWin() const {
  return row_win;
}

void Game::setBoardSize(int boardSize) {
  size_of_map = boardSize;
}

void Game::setRowWin(int rowWin) {
  row_win = rowWin;
}

Game::~Game() {
  delete[]field;
}

void Game::print_board() { // wyswietlanie mapy
  for (int k = 0; k < size_of_map; ++k) {
    std::cout << "----";
  }
  std::cout << "-\n";
  for (int i = 0; i < size_of_map; ++i) {
    std::cout << "|";
    for (int j = 0; j < size_of_map; ++j) {
      std::cout << " ";
      if (field[j * size_of_map + i] == 0) {
        std::cout << " ";
      } else if (field[j * size_of_map + i] == 1) {
        std::cout << "X";
      } else if (field[j * size_of_map + i] == 2) {
        std::cout << "O";
      }
      std::cout << " |";
    }
    std::cout << "\n";
    for (int k = 0; k < size_of_map; ++k) {
      std::cout << "----";
    }
    std::cout << "-\n";
  }
}

bool Game::play_tour(int i, int j, bool bot) { // runda gracza
  if (i < 0 || j < 0 || i >= size_of_map || j >= size_of_map) // warunki podstawowe
    return false;
  if (get_element_value(i, j) != 0)
    return false;

  if (bot)
    get_element(i, j) = 2;
  else
    get_element(i, j) = 1;
  return false;
}

int & Game::get_element(int i, int j) {
  return field[size_of_map * i + j];
}

int Game::get_element_value(int i, int j) {
  return field[size_of_map * i + j];
}



bool Game::check_horizontal(bool bot) { // sprawdzamy wiersz po wierszu
  int symbol = bot ? 2 : 1;
  for (int i = 0; i < size_of_map; ++i) {
    int line = 0;
    for(int j = 0; j < size_of_map; j++){
      if (get_element_value(i, j) == symbol) {
        line++;
      } else if (get_element_value(i, j) != symbol) {
        line = 0;
      } else if (get_element_value(i, j) && size_of_map - j < row_win){
        break;
      }
      if (line >= row_win) {
        return true;
      }
    }
  }
  return false;
}

bool Game::check_vertical(bool bot) { // sprawdzamy kolumna po kolumnie
  int symbol = bot ? 2 : 1;
  for (int j = 0; j < size_of_map; ++j) {
    int line = 0;
    for(int i = 0; i < size_of_map; i++){
      if (get_element_value(i, j) == symbol) {
        line++;
      } else if (get_element_value(i, j) != symbol) {
        line = 0;
      } else if (get_element_value(i, j) && size_of_map - j < row_win){
        break;
      }
      if (line >= row_win) {
        return true;
      }
    }
  }
  return false;
}

bool Game::check_diagonal(bool isBot) {
  int symbol = isBot ? 2 : 1; // gdy jest bot to ma symbol 2, gdy gracz ma symbol 1
  List win; // lista wygranych
  for (int i = 0; i < row_win; ++i)
    win.append(symbol); // dodajemy wygrane


  for (int i = 0; i < size_of_map - row_win + 1; ++i) { // petla sprawdzania diagonalnych
    // 1 sprawdzanie na jeden skos
    List diagonal;
    int x = i;
    int y = 0;

    for (int j = 0; j < size_of_map - i; ++j) {
      diagonal.append(get_element_value(x, y));
      x++;
      y++;
    }
    for (int j = 0; j < diagonal.getSize() - win.getSize() + 1; ++j) {
      // j: j + row_win
      bool tmp = true;
      for (int k = j; k < j + row_win; ++k) {
        if (win[0] != diagonal[k]) { // jezeli diagonala od j:j+row_win NIE jest jednego znaku
          tmp = false;
          continue;
        }
      }
      if (tmp) return true; // jezeli jest wygrana
    }
    // 2 sprawdzanie kolejny skos
    diagonal = List();
    x = 0;
    y = i;

    for (int j = 0; j < size_of_map - i; ++j) {
      diagonal.append(get_element_value(x, y));
      x++;
      y++;
    }
    for (int j = 0; j < diagonal.getSize() - win.getSize() + 1; ++j) {
      // j: j + row_win
      bool tmp = true;
      for (int k = j; k < j + row_win; ++k) {
        if (win[0] != diagonal[k]) { // jezeli diagonala od j:j+row_win NIE jest jednego znaku
          tmp = false;
          continue;
        }
      }
      if (tmp) return true; // jezeli jest wygrana
    }

    // 3 sprawdzanie lewo, mniejsze skosy
    diagonal = List();
    x = size_of_map - 1 - i;
    y = 0;

    for (int j = 0; j < size_of_map - i; ++j) {
      diagonal.append(get_element_value(x, y));
      x++;
      y++;
    }
    for (int j = 0; j < diagonal.getSize() - win.getSize() + 1; ++j) {
      // j: j + row_win
      bool tmp = true;
      for (int k = j; k < j + row_win; ++k) {
        if (win[0] != diagonal[k]) { // jezeli diagonala od j:j+row_win NIE jest jednego znaku
          tmp = false;
          continue;
        }
      }
      if (tmp) return true; // jezeli jest wygrana
    }

    // 4 sprawdzanie prawo mniejsze skosy
    diagonal = List();
    x = size_of_map - 1;
    y = 0 + i;

    for (int j = 0; j < size_of_map - i; ++j) {
      diagonal.append(get_element_value(x, y));
      x++;
      y++;
    }
    for (int j = 0; j < diagonal.getSize() - win.getSize() + 1; ++j) {
      // j: j + row_win
      bool tmp = true;
      for (int k = j; k < j + row_win; ++k) {
        if (win[0] != diagonal[k]) { // jezeli diagonala od j:j+row_win NIE jest jednego znaku
          tmp = false;
          continue;
        }
      }
      if (tmp) return true; // jezeli jest wygrana
    }
  }
  return false; // nie ma wygranej
}

bool Game::check_game_finish() { // gra sie konczy gdy wszystkie pola są rozne od 0, czyli wypelnione
  for (int i = 0; i < size_of_map; ++i) {
    for (int j = 0; j < size_of_map; ++j){
      if (get_element_value(i, j) == 0) // sprawdzam czy pole wypelnione
        return false;
    }
  }
  return true;
}

bool Game::check_win(bool bot) {
  return check_horizontal(bot) || check_vertical(bot) || check_diagonal(bot);
}

void Game::player_tour() {
  int i = 0;
  int j = 0;

  do {
    std::cout << "Podaj wiersz i kolumne: ";
    std::cin >> i >> j;
    i--;
    j--;
  } while (get_element_value(i, j) != 0);
  play_tour(i, j, false);
}

