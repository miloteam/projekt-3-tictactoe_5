#include <iostream>
#include <Bot.h>
#include "Game.h"

int main()
{
  int size, win_row;
  std::cout << "Podaj rozmiar gry: ";
  std::cin >> size;
  std::cout << "Podaj ilość wymaganaych pól do wygranej: ";
  std::cin >> win_row;


  Game board(size, win_row);
  Bot bot(board.getBoardSize(), 6);
  board.print_board();
  do {
    board.player_tour();
    board.print_board();
    bot.bot_tour(board);
    board.print_board();
  } while (!board.check_win(false) and !board.check_win(true) && !board.check_game_finish());

  if (board.check_win(false)) {
    std::cout << "Wygrywa gracz! \n";
    return 0;
  } else if (board.check_win(true)) {
    std::cout << "Wygrywa bot! \n";
    return 0;
  } else if (board.check_game_finish()) {
    std::cout << "Remis! \n";
    return 0;
  }
}

