#include <cmath>
#include "Bot.h"
#include <bits/stdc++.h>

void Bot::bot_tour(Game & board) {
  int move_i = 0;
  int move_j = 0;
  double best_score = INT_MIN;
  for (int i = 0; i < board.getBoardSize(); ++i) {
    for (int j = 0; j < board.getBoardSize(); ++j) {
      if (board.get_element_value(i, j) == 0) {
        board.get_element(i, j) = 2;
        double minimax_score = minimax(board, true, max_depth, INT_MIN, INT_MAX);
        board.get_element(i, j) = 0;
        if (minimax_score > best_score) {
          best_score = minimax_score;
          move_i = i;
          move_j = j;
        }
      }
    }
  }
  board.play_tour(move_i, move_j, true); // zagrywamy najlepszy ruch
  std::cout << "Bot zagrywa ruch: " << move_i + 1 << ", " << move_j + 1 << "\n";
}

Bot::Bot(int size, int max_deph) { // konstuktor
  this->size_of_board = size;
  this->max_depth = max_deph;
}

double Bot::minimax(Game& board, bool bot, int depth, double alfa, double beta) {

  if (board.check_win(bot) || board.check_win(!bot)){ // gdy gre wygrywa bot
    if (bot) // zwracamy maxa dla bota = PEWNA WYGRANA
      return INT_MAX;
    else // zwracamy min - pewna przegrana
      return INT_MIN;
  }

  if (board.check_game_finish() || depth == 0){ // gdy depth =0 lub gra sie zakonczyla poprzez remis
    if (bot)
      return calculate(board);
    else
      return (-1) * calculate(board);
  }

  double best_score;

  if (bot) {
    bot = false;
    best_score = INT_MAX;
  } else {
    bot = true;
    best_score = INT_MIN;
  }

  int symbol = bot ? 2 : 1;

  for (int i = 0; i < size_of_board; ++i) {
    for (int j = 0; j < size_of_board; ++j) {
      if (board.get_element_value(i,j) == 0) {
        if (bot) {
          board.get_element(i, j) = symbol;
          double m = minimax(board, bot, depth - 1, alfa, beta);
          if (best_score < m)
            best_score = m;
          if (alfa < best_score)
            alfa = best_score;
          board.get_element(i, j) = 0;
          if (alfa >= beta)
            return best_score;
        } else { // human
          board.get_element(i, j) = symbol;
          double m = minimax(board, bot, depth - 1, alfa, beta);
          if (best_score > m)
            best_score = m;
          if (beta > best_score)
            beta = best_score;
          board.get_element(i, j) = 0;
          if (alfa >= beta)
            return best_score;
        }
      }
    }
  }
  return best_score;
}

double Bot::calculate(Game &board) { // liczenie wyniku
  double score = 0;
  for (int i = 0; i < size_of_board; ++i) {
    double tmp = 0;
    for (int j = 0; j < size_of_board; ++j) {
      if (board.get_element_value(i, j) == 2) // bot
      {
        tmp++;
      }
      else if (board.get_element_value(i, j) == 0) // empty
      {
        tmp = 0;
        break;
      }
    }
    if (tmp != 0)
      score += pow(10, tmp);
  }

  for (int i = 0; i < size_of_board; ++i) {
    double tmp = 0;
    for (int j = 0; j < size_of_board; ++j) {
      if (board.get_element_value(j, i) == 2) { // bot
        tmp++;
      } else if (board.get_element_value(j, i) == 0){
        tmp = 0;
        break;
      }
    }
    if (tmp != 0)
      score += pow(10, tmp);
  }

  double tmp = 0;
  for (int i = 0; i < size_of_board; ++i) {
    if (board.get_element_value(i,i) == 2) { // bot
      tmp++;
    } else if (board.get_element_value(i,i) == 0){
      tmp = 0;
      break;
    }
  }
  if (tmp != 0)
    score += pow(10, tmp);

  tmp = 0;
  for (int i = 1; i < size_of_board; ++i){
    if (board.get_element_value(size_of_board - i, i - 1) == 2) { // bot
      tmp++;
    } else if (board.get_element_value(size_of_board - i, i - 1) != 2 &&
               board.get_element_value(size_of_board - i, i - 1) != 0){
      tmp = 0;
      break;
    }
  }
  if (tmp != 0)
    score += pow(10, tmp);
  return score;
}
