#pragma once
#include "Game.h"

class Bot {
public:
  Bot(int board_size, int max_deph);
  void bot_tour(Game & board);
private:
  int size_of_board;
  int max_depth = 5;
  double minimax(Game& board, bool bot, int depth, double alfa, double beta); // algorytm minimaxa
  double calculate(Game & board); // liczenie wartosci
};